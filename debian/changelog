seqkit (2.8.2+ds-1) UNRELEASED; urgency=medium

  * Team upload.
  * New upstream version 2.8.2
  * Patch to use original bsipis/thist rather than botond-sipos/thist fork
  * Patch to use vbauerster/mpb v8 (latest) instead of v5

 -- Maytham Alsudany <maytha8thedev@gmail.com>  Sat, 22 Jun 2024 19:39:51 +0800

seqkit (2.3.1+ds-2) unstable; urgency=medium

  * Update Build-Depends, fix with cme (Closes: #1058558)
  * Add d/clean to cleanup generate artifacts (Closes: #1046668)

 -- Nilesh Patra <nilesh@debian.org>  Mon, 25 Dec 2023 22:26:02 +0530

seqkit (2.3.1+ds-1) unstable; urgency=medium

  * Really fix watch file
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 29 Sep 2022 13:32:06 +0200

seqkit (2.3.0+ds-1) unstable; urgency=medium

  * Fix watchfile to detect new versions on github
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 25 Aug 2022 18:16:54 +0200

seqkit (2.2.0+ds-2) unstable; urgency=medium

  * Team upload.
  * Ignore test results on ppc and s390
  * Add populate_shell_completions.sh script to
    automate populating shell completions
  * Update bash shell completion

 -- Nilesh Patra <nilesh@debian.org>  Sun, 29 May 2022 04:28:29 +0000

seqkit (2.2.0+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2.2.0+ds
  * Bump Standards-Version to 4.6.1 (no changes needed)
  * Drop fix-version.patch

 -- Nilesh Patra <nilesh@debian.org>  Sat, 28 May 2022 12:50:23 +0530

seqkit (2.1.0+ds-1) unstable; urgency=medium

  * New upstream version 2.1.0+ds
  * Tighten B-D on golang-github-shenwei356-bio-dev
  * Drop spellings.patch
  * d/p/fix-version.patch: Set to right version (2.1.0)
  * Update all shell_completions
  * Update manpage

 -- Nilesh Patra <nilesh@debian.org>  Sun, 16 Jan 2022 13:11:43 +0000

seqkit (2.0.1+ds-2) unstable; urgency=medium

  * d/shell_completions/*, d/install: Add shell completions
    for bash, zsh and fish
  * d/p/fix-version.patch: Fix version to 2.0.1
  * Update manpages
  * d/control: Update versioned B-D on shenwei356-bio

 -- Nilesh Patra <nilesh@debian.org>  Mon, 27 Sep 2021 02:02:18 +0530

seqkit (2.0.1+ds-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * Refresh patches

 -- Nilesh Patra <nilesh@debian.org>  Sun, 26 Sep 2021 23:48:31 +0530

seqkit (2.0.0+ds-1) unstable; urgency=medium

  * New upstream version 2.0.0+ds
  * Refresh patches
  * Improve autopkgtests now that ssshtest is in archive

 -- Nilesh Patra <nilesh@debian.org>  Sun, 29 Aug 2021 18:17:29 +0530

seqkit (0.16.1+ds-1) unstable; urgency=medium

  * d/watch: Fix watch regex
  * New upstream version 0.16.0+ds
  * d/p/spellings.patch: Refresh patch
  * Add B-D on golang-github-twotwotwo-sorts-dev
  * Tighten B-D on golang-github-shenwei356-bio-dev
    and golang-github-shenwei356-bwt-dev

 -- Nilesh Patra <nilesh@debian.org>  Tue, 17 Aug 2021 20:42:55 +0530

seqkit (0.15.0+ds-2) unstable; urgency=medium

  * d/watch: do not rank rc versions higher than real releases
  * No tab in license text (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 27 Jan 2021 18:10:17 +0100

seqkit (0.15.0+ds-1) unstable; urgency=medium

  * New upstream version 0.15.0+ds
  * Refresh patch
  * Update manpage
  * Declare compliance with policy 4.5.1

 -- Nilesh Patra <npatra974@gmail.com>  Thu, 14 Jan 2021 11:17:52 +0000

seqkit (0.14.0+ds-1) unstable; urgency=medium

  * New upstream version 0.14.0+ds
  * Refresh patch
  * Update manpages.

 -- Nilesh Patra <npatra974@gmail.com>  Sat, 31 Oct 2020 00:57:33 +0530

seqkit (0.13.2+ds-1) unstable; urgency=medium

  * New upstream version 0.13.2+ds
  * Update Build Deps
  * Refresh patch

 -- Nilesh Patra <npatra974@gmail.com>  Thu, 29 Oct 2020 17:48:27 +0530

seqkit (0.12.1+ds-2) unstable; urgency=medium

  * Souce-only-upload

 -- Nilesh Patra <npatra974@gmail.com>  Mon, 19 Oct 2020 13:30:32 +0530

seqkit (0.12.1+ds-1) unstable; urgency=medium

  * Initial release (Closes: #967051)

 -- Nilesh Patra <npatra974@gmail.com>  Mon, 03 Aug 2020 21:49:28 +0530
